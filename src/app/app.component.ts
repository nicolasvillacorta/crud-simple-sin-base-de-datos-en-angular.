import { Component } from '@angular/core';
import { Employee } from './models/employee';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  employeeArray: Employee[] = [
    {id: 1, name: "Ryan", country: "USA" },
    {id: 2, name: "Angelica", country: "USA" },
    {id: 3, name: "Joe", country: "USA" },
  ];

  selectedEmployee: Employee = new Employee();

  
  openForEdit(employee: Employee){
    this.selectedEmployee = employee;
  }

  addOrEdit(){
    if(this.selectedEmployee.id === 0){
      this.selectedEmployee.id = this.employeeArray.length + 1;
      this.employeeArray.push(this.selectedEmployee);
    }
   

    this.selectedEmployee = new Employee();
  }

  delete(){
    //El confirm, tira la alerta, si das si, entra por el if, sino por el else.
    if(confirm('Are you sure you want to delete it?')){
      //filter devuelve un nuevo array con los elementos que cumplann la condicion,
      //cuando un elemento es distinto al selectedEmployee, lo mete al nuevo array,
      //al tocar el distinto, no lo mete al array.
      this.employeeArray = this.employeeArray.filter(x => x != this.selectedEmployee);
      //Setea selectedEmployee en blanco denuevo.
      this.selectedEmployee = new Employee();
    }
  }

}
